﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaintenanceLayout.master" AutoEventWireup="true" CodeFile="NewCustomer.aspx.cs" Inherits="NewCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    
    <asp:Label ID="lblContactName" runat="server" Text="Contact Name: "></asp:Label>
    <asp:TextBox ID="txtContactName" runat="server" style="margin-left: 22px" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must enter a Contact Name"
    ControlToValidate="txtContactName"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblCompanyName" runat="server" Text="Company Name: "></asp:Label>
    <asp:TextBox ID="txtCompanyName" runat="server" style="margin-left: 9px" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must enter a Company Name"
    ControlToValidate="txtCompanyName"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblContactTitle" runat="server" Text="Contact Title: "></asp:Label>
    <asp:TextBox ID="txtContactTitle" runat="server" style="margin-left: 35px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You must enter a Contact Title"
    ControlToValidate="txtContactTitle"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblPhone" runat="server" Text="Phone Number: "></asp:Label>
    <asp:TextBox ID="txtPhone" runat="server" style="margin-left: 18px" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="You must enter a Phone Number"
    ControlToValidate="txtPhone"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblFax" runat="server" Text="Fax Number: "></asp:Label>
    <asp:TextBox ID="txtFax" runat="server" style="margin-left: 37px" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="You must enter a Fax Number"
    ControlToValidate="txtFax"></asp:RequiredFieldValidator>
    <br />
    <asp:Button ID="btnSubmitNewCustomer" runat="server" Text="Create" 
        style="margin-left: 130px" 
        onclick="btnSubmitNewCustomer_Click" />
</asp:Content>