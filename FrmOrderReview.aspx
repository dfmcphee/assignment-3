<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CustomerLayout.master" CodeFile="FrmOrderReview.aspx.cs" Inherits="FrmOrderReview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
        <h3>Your cart contains:</h3>
       <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
        <br style="clear:both"/>
        <asp:Label ID="lblFeedback" runat="server" ForeColor="#CC0000"></asp:Label>
        <br /><br />
       <asp:Button ID="btnConfirm" runat="server" OnClick="btnConfirm_Click" Text="Confirm Order" CausesValidation="False" />
       <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
       <br />
       <asp:Button ID="btnReturn" runat="server" OnClick="btnReturn_Click" Text="Return to Catalog" />
</asp:Content>