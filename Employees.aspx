﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MaintenanceLayout.master" CodeFile="Employees.aspx.cs" Inherits="Employees" %>
    
<asp:content id="Content1" ContentPlaceHolderID="head" runat="server">
</asp:content>

<asp:content id="Content2" ContentPlaceHolderID="content" runat="server">
    <asp:GridView ID="gvEmployees" runat="server" 
            OnRowEditing="gvEmployees_EditCommand" 
            OnRowUpdating="gvEmployees_UpdateCommand" 
            OnRowCancelingEdit="gvEmployees_CancelCommand"
            AutoGenerateColumns="False" DataKey="EmployeeID" BackColor="White" 
            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
            ForeColor="Black" GridLines="Vertical">
            
            <RowStyle BackColor="#F7F7DE" />
            
            <Columns>
                <asp:CommandField ButtonType="Button" ShowEditButton="True" />
                <asp:BoundField DataField="EmployeeID" ReadOnly="True" HeaderText="Employee ID" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="Title" HeaderText="Title" />
                <asp:BoundField DataField="Address" HeaderText="Address" />
                <asp:BoundField DataField="City" HeaderText="City" />
                <asp:BoundField DataField="Region" HeaderText="Region" />
                <asp:BoundField DataField="PostalCode" HeaderText="PostalCode" />
                <asp:BoundField DataField="HomePhone" HeaderText="Home Phone" /> 
                <asp:BoundField DataField="BirthDate" ReadOnly="True" HeaderText="DOB" />
                <asp:BoundField DataField="HireDate" ReadOnly="True" HeaderText="Hire Date" />  
            </Columns>
            
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
            
        </asp:GridView>
        <asp:Button ID="btnNewEmployee" runat="server" Text="New" 
        style="margin-left: 5px; margin-top: 15px" onclick="btnNewEmployee_Click" 
        onclientclick="btnNewEmployee_Click" />
</asp:content>