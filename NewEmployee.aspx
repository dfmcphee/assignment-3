﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaintenanceLayout.master" AutoEventWireup="true" CodeFile="NewEmployee.aspx.cs" Inherits="NewEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    
    <asp:Label ID="lblLastName" runat="server" Text="Last Name: "></asp:Label>
    <asp:TextBox ID="txtLastName" runat="server" style="margin-left: 24px" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must enter a Last Name"
    ControlToValidate="txtLastName"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblFirstName" runat="server" Text="First Name: "></asp:Label>
    <asp:TextBox ID="txtFirstName" runat="server" style="margin-left: 23px" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must enter a First Name"
    ControlToValidate="txtFirstName"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblTitle" runat="server" Text="Title: "></asp:Label>
    <asp:TextBox ID="txtTitle" runat="server" style="margin-left: 71px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You must enter a Title"
    ControlToValidate="txtTitle"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblAddress" runat="server" Text="Address: "></asp:Label>
    <asp:TextBox ID="txtAddress" runat="server" style="margin-left: 41px" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="You must enter an Address"
    ControlToValidate="txtAddress"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblCity" runat="server" Text="City: "></asp:Label>
    <asp:TextBox ID="txtCity" runat="server" style="margin-left: 73px" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="You must enter a City"
    ControlToValidate="txtCity"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblRegion" runat="server" Text="Region: "></asp:Label>
    <asp:TextBox ID="txtRegion" runat="server" style="margin-left: 50px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="You must enter a Region"
    ControlToValidate="txtRegion"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblPostalCode" runat="server" Text="Postal Code: "></asp:Label>
    <asp:TextBox ID="txtPostalCode" runat="server" style="margin-left: 13px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="You must enter a Postal Code"
    ControlToValidate="txtPostalCode"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblHomePhone" runat="server" Text="Home Phone: "></asp:Label>
    <asp:TextBox ID="txtHomePhone" runat="server" style="margin-left: 9px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="You must enter a Home Phone Number"
    ControlToValidate="txtHomePhone"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblExtension" runat="server" Text="Extension: "></asp:Label>
    <asp:TextBox ID="txtExtension" runat="server" style="margin-left: 32px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="You must enter an Extension"
    ControlToValidate="txtExtension"></asp:RequiredFieldValidator>
    <br />
    <asp:Button ID="btnSubmitNewEmployee" runat="server" Text="Create" 
        style="margin-left: 108px" 
        onclick="btnSubmitNewEmployee_Click" />
</asp:Content>