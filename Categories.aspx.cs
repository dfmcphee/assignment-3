﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Categories : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if ((String)Session["Role"] == "Sales Representative")
        {
            Response.Redirect("Maintenance.aspx", false);
        }

        else if (!IsPostBack)
        {
            gvCategories.BorderStyle = BorderStyle.Outset;
            gvCategories.BorderWidth = Unit.Pixel(3);

            DataTable dt;
            string selectionString = "SELECT CategoryID, CategoryName, Description FROM Categories";

            dt = ((DataAccessLayer)Application["DBAccess"]).GetTable(selectionString);

            gvCategories.DataSource = dt;
            gvCategories.DataBind();

            //Persist the table in the Session object.
            Session["dtCategories"] = dt;
        }

        else
        {
            //Retrieve the table from the session object.
            DataTable dt = (DataTable)Session["dtCategories"];
            gvCategories.DataSource = dt;
        }
    }

    public void gvCategories_EditCommand(object source, GridViewEditEventArgs e)
    {
        //Set the edit index.
        gvCategories.EditIndex = e.NewEditIndex;
        //Bind data to the GridView control.
        gvCategories.DataBind();
    }

    public void gvCategories_UpdateCommand(object source, GridViewUpdateEventArgs e)
    {
        GridViewRow row = gvCategories.Rows[e.RowIndex];

        // Make change to database.
        string updateString = "UPDATE Categories SET "
            + "CategoryName=" + (((TextBox)(row.Cells[2].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[2].Controls[0])).Text + "'" : "null")
            + ", Description=" + (((TextBox)(row.Cells[3].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[3].Controls[0])).Text + "'" : "null")
            + " WHERE CategoryID=" + row.Cells[1].Text;

        ((DataAccessLayer)Application["DBAccess"]).RunQuery(updateString);

        // Update data table.
        DataTable dt = (DataTable)Session["dtCategories"];
        dt.Rows[row.DataItemIndex]["CategoryName"] = ((TextBox)(row.Cells[2].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["Description"] = ((TextBox)(row.Cells[3].Controls[0])).Text;

        // Switch out of edit mode.
        gvCategories.EditIndex = -1;

        // Bind data.
        gvCategories.DataSource = dt;
        gvCategories.DataBind();

        //Persist the table in the Session object.
        Session["dtCategories"] = dt;
    }

    public void gvCategories_CancelCommand(object source, GridViewCancelEditEventArgs e)
    {
        // Switch out of edit mode.
        gvCategories.EditIndex = -1;

        gvCategories.DataBind();
    }
    protected void btnNewCategory_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewCategory.aspx", true);
    }
}
