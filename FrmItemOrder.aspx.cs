/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


// Note: TextBox6 and txtQuantity have been right aligned
// as is appropriate for the presentation of numbers
// This is done using the Format|Style menu item
public partial class FrmItemOrder : System.Web.UI.Page
{
    // A way to simplify other code
    // These are references to arrays of references
    private Label[] labelArray = null;
    private TextBox[] textBoxArray = null;
    private bool doReplace = false;
    private bool doAdd = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentItem"] == null)
        {
            Response.Redirect("CatalogDisplay.aspx");
        }
        else
        {
            InitializeArrays();

            // Query string concatenates fixed text with the user's selection
            string selectionString =
                "SELECT Products.ProductID, " +
                    "Products.ProductName, Categories.CategoryName, " +
                    "Suppliers.CompanyName, Products.QuantityPerUnit," +
                    "Products.UnitPrice, Products.UnitsInStock " +
                    "FROM Suppliers INNER JOIN (Categories  " +
                    "INNER JOIN Products ON  " +
                    "Categories.CategoryID = Products.CategoryID)  " +
                    "ON Suppliers.SupplierID = Products.SupplierID " +
                    "WHERE Products.ProductID = " +
                    int.Parse(Session["CurrentItem"].ToString());

            DataTable dt = ((DataAccessLayer)Application["DBAccess"]).GetTable(selectionString);

            // Make sure we have the data we expect
            // i.e. at least a single row of data
            if (dt.Columns.Count != 0 &&
                dt.Rows.Count != 0)
            {
                for (int index = 0; index < dt.Columns.Count; index++)
                {
                    // using the arrays lets this be 4 lines instead of the whole commented block below
                    labelArray[index].Text = dt.Columns[index].ColumnName;
                    if (index == 5) // The price field
                    {
                        // Format the price as Fixed decimal
                        // A bit tricky here
                        // I tried the same with a cast to double
                        // and got a run time exception that said
                        // my value MUST be less than infinity
                        // Decimal works fine - it does agree with the MSAccess data type
                        textBoxArray[index].Text = ((Decimal)dt.Rows[0][index]).ToString("F");
                    }
                    else
                    {
                        textBoxArray[index].Text = dt.Rows[0][index].ToString();
                    }

                }// end of for loop

                //generate page title
                Page.Title = textBoxArray[3].Text + " - " + textBoxArray[1].Text;
                Session["stock" + TextBox1.Text] = int.Parse(txtStock.Text);
            }
            int id = int.Parse(Session["CurrentItem"].ToString());
            if (Session["cart"] != null && ((DataTable)Session["cart"]).Rows.Contains(id))
            {
                DataRow order = ((DataTable)Session["cart"]).Rows.Find(new object[] { id });
                btnOrder.Visible = false;
                btnMerge.Visible = true;
                btnAdd.Visible = true;
                CurrentOrder.Visible = true;
                txtCurrentOrder.Text = order["quantity"].ToString();
            }
            else
            {
                btnOrder.Visible = true;
                btnMerge.Visible = false;
                btnAdd.Visible = false;
                CurrentOrder.Visible = false;
            }

            // Set the user Focus on  the textbox they need to use
            txtQuantity.Focus();
        }
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        // Clear the current item
        Session.Remove("CurrentItem");
        // Go back to catalog form
        Server.Transfer("CatalogDisplay.aspx");
    }
    protected void btnOrder_Click(object sender, EventArgs e)
    {
        //Check for Shoppingcart object
        // Create first if not there
        DataTable cart;
        if (Session["cart"] == null){
            cart = new DataTable();
            cart.Columns.Add("id");
            cart.Columns.Add("name");
            cart.Columns.Add("price");
            cart.Columns.Add("quantity");
            cart.PrimaryKey = new DataColumn[]{cart.Columns["id"]};
            Session["cart"] = cart;
        }else {
            cart = (DataTable)Session["cart"];
        }
        int quantity = 0;
        int id = int.Parse(TextBox1.Text);

        // make sure there is text
        if (txtQuantity.Text.Trim().Length != 0)
        {
            // try to convert text to int  ???
            int stock = int.Parse(txtStock.Text);
            if (!int.TryParse(txtQuantity.Text, out quantity))
            {
                lblFeedback.Text = "You must enter a number for the number of units to order.";
                return;
            }
            if (stock < quantity)
            {
                lblFeedback.Text = "There are not that many available.";
                return;
            }
            if (quantity < 0)
            {
                lblFeedback.Text = "The order must be a positive number. Unless, of course, you want to pay to ship it to us instead?";
                return;
            }
            if (quantity == 0)
            {
                lblFeedback.Text = "You entered 0. You can't really order 0. That would mean you want to order the shipping box.";
                return;
            }
            // check for this item in the cart
            // Note this only makes sense if the "cart" exists
            // since it checks for an individual item in the cart
            if (cart.Rows.Contains(id))
            {
                DataRow order = cart.Rows.Find(new object[]{id});
                if (doAdd)
                {
                    if (stock < int.Parse(order["quantity"].ToString()) + quantity)
                    {
                        lblFeedback.Text = "There are not that many available.";
                        return;
                    }
                    order["quantity"] = int.Parse(order["quantity"].ToString()) + quantity;
                }
                else if (doReplace)
                {
                    if (stock < quantity)
                    {
                        lblFeedback.Text = "There are not that many available.";
                        return;
                    }
                    order["quantity"] = quantity;
                }
            }
            else  // This is a new item
            {
                // Make the item
                DataRow item = cart.NewRow();
                item["id"] = id;
                item["name"] = TextBox2.Text;
                item["price"] = double.Parse(TextBox6.Text);
                item["quantity"] = int.Parse(txtQuantity.Text);
                // add to cart  
                cart.Rows.Add(item);

            }
            // How to make this work ??
            // Who is the sender ?
            // What are the System.EventArgs ?
            this.btnReturn_Click(sender, e);
        }
        else
        {
            lblFeedback.Text = "Nothing Ordered<br>You must order some of the product or return to the Catalog";
        }
    }
    private void InitializeArrays()
    {
        // First create arrays of 6 references
        // No Label or TextBox objects are created
        // They already exist, we are simply going to give them another set of names
        // that are array elements
        labelArray = new Label[7];
        textBoxArray = new TextBox[7];

        // Now assign the Controls to their array references
        labelArray[0] = Label1;
        labelArray[1] = Label2;
        labelArray[2] = Label3;
        labelArray[3] = Label4;
        labelArray[4] = Label5;
        labelArray[5] = Label6;
        labelArray[6] = LabelStock;

        textBoxArray[0] = TextBox1;
        textBoxArray[1] = TextBox2;
        textBoxArray[2] = TextBox3;
        textBoxArray[3] = TextBox4;
        textBoxArray[4] = TextBox5;
        textBoxArray[5] = TextBox6;
        textBoxArray[6] = txtStock;

        // Now we can manipulate the controls using the arrays
    } // end of InitializeArrays
    protected void btnReplace_Click(object sender, EventArgs e)
    {
        doReplace = true;
        this.btnOrder_Click(sender, e);
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        doAdd = true;
        this.btnOrder_Click(sender, e);
    }
}
