﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class NewShipper : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if ((String)Session["Role"] == "Sales Representative")
        {
            Response.Redirect("Maintenance.aspx", false);
        }

        else if (!IsPostBack)
        {
            
        }

        else
        {
            
        }
    }

    protected void btnSubmitNewShipper_Click(object sender, EventArgs e)
    {
        if(txtCompanyName.Text != "" && txtPhone.Text != ""){
            // Make change to database.
            string insertString = "INSERT INTO Shippers (CompanyName, Phone) VALUES "
                + "('" + txtCompanyName.Text
                + "', '" + txtPhone.Text + "');";

            string shipperID = (((DataAccessLayer)Application["DBAccess"]).InsertIndex(insertString)).ToString();

            // Update data table.
            DataTable dt = (DataTable)Session["dtShippers"];
            DataRow newShipperRow = dt.NewRow();
            newShipperRow["ShipperID"] = shipperID;
            newShipperRow["CompanyName"] = txtCompanyName.Text;
            newShipperRow["Phone"] = txtPhone.Text;
            dt.Rows.Add(newShipperRow);

            //Persist the table in the Session object.
            Session["dtShippers"] = dt;

            //redirect back to shipper page
            Response.Redirect("Shippers.aspx", true);
        }
        else{
            //invalid entry
            if (txtCompanyName.Text == "")
            {
                txtCompanyName.Focus();
            }
            else
            {
                txtPhone.Focus();
            }

        }
    }
}
