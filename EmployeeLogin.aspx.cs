﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private bool SiteSpecificAuthenticationMethod(string UserName, string Password)
    {
        String query = "SELECT * FROM Employees" +
                         " WHERE Username='" + UserName +
                         "' AND Password ='" + Password + "';";

        object result = ((DataAccessLayer)Application["DBAccess"]).GetObject(query);

        if (result != null)
        {
            DataRow row = ((DataAccessLayer)Application["DBAccess"]).GetRow(query);
            Session["First Name"] = row[2];
            Session["Role"] = row[3];
            return true;
        }
        else
        {
            return false;
        }
    }

    public void OnLoggedIn(object sender, EventArgs e)
    {
        Response.Redirect("Maintenance.aspx", false);
    }

    protected void OnAuthenticate(object sender, AuthenticateEventArgs e)
    {
        bool authenticated = false;
        authenticated = SiteSpecificAuthenticationMethod(Login1.UserName, Login1.Password);
        
        e.Authenticated = authenticated;
    }
}

