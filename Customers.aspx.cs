﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Customers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }
        
        else if (!IsPostBack)
        {
            gvCustomers.BorderStyle = BorderStyle.Outset;
            gvCustomers.BorderWidth = Unit.Pixel(3);

            DataTable dt;
            string selectionString = "SELECT CustomerID, ContactName, CompanyName, "
            + "ContactTitle, Phone, Fax FROM Customers";

            dt = ((DataAccessLayer)Application["DBAccess"]).GetTable(selectionString);

            gvCustomers.DataSource = dt;
            gvCustomers.DataBind();

            //Persist the table in the Session object.
            Session["dtCustomers"] = dt;
        }
        else
        {
            //Retrieve the table from the session object.
            DataTable dt = (DataTable)Session["dtCustomers"];
            gvCustomers.DataSource = dt;
        }
    }

    public void gvCustomers_EditCommand(object source, GridViewEditEventArgs e)
    {
        //Set the edit index.
        gvCustomers.EditIndex = e.NewEditIndex;
        //Bind data to the GridView control.
        gvCustomers.DataBind();
    }

    public void gvCustomers_UpdateCommand(object source, GridViewUpdateEventArgs e)
    {
        GridViewRow row = gvCustomers.Rows[e.RowIndex];

        // Make change to database.
        string updateString = "UPDATE Customers SET "
            + "ContactName=" + (((TextBox)(row.Cells[2].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[2].Controls[0])).Text + "'" : "null")
            + ", CompanyName=" + (((TextBox)(row.Cells[3].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[3].Controls[0])).Text + "'" : "null")
            + ", ContactTitle=" + (((TextBox)(row.Cells[4].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[4].Controls[0])).Text + "'" : "null")
            + ", Phone=" + (((TextBox)(row.Cells[5].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[5].Controls[0])).Text + "'" : "null")
            + ", Fax=" + (((TextBox)(row.Cells[6].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[6].Controls[0])).Text + "'" : "null")
            + " WHERE CustomerID='" + row.Cells[1].Text + "'";

        ((DataAccessLayer)Application["DBAccess"]).RunQuery(updateString);

        // Update data table.
        DataTable dt = (DataTable)Session["dtCustomers"];
        dt.Rows[row.DataItemIndex]["ContactName"] = ((TextBox)(row.Cells[2].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["CompanyName"] = ((TextBox)(row.Cells[3].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["ContactTitle"] = ((TextBox)(row.Cells[4].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["Phone"] = ((TextBox)(row.Cells[5].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["Fax"] = ((TextBox)(row.Cells[6].Controls[0])).Text;

        // Switch out of edit mode.
        gvCustomers.EditIndex = -1;

        // Bind data.
        gvCustomers.DataSource = dt;
        gvCustomers.DataBind();

        //Persist the table in the Session object.
        Session["dtCustomers"] = dt;
    }

    public void gvCustomers_CancelCommand(object source, GridViewCancelEditEventArgs e)
    {
        // Switch out of edit mode.
        gvCustomers.EditIndex = -1;

        gvCustomers.DataBind();
    }
    protected void btnNewCustomer_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewCustomer.aspx", true);
    }
}
