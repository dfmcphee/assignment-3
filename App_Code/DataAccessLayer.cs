﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.OleDb;

//adding delegeate???? where???
public delegate void EventTableChangeHandler(object sender, EventArgs e);


/// <summary>
/// Summary description for DataAccessLayer
/// </summary>
public class DataAccessLayer
{
    //adding event
    public event EventTableChangeHandler TableChangeEvent;

    private string connectionstring;
    private OleDbConnection connection;
    private bool isOpen;

    /// <summary>
    /// Can't have no connectionstring, so hide the constructor with no parameters
    /// </summary>
	private DataAccessLayer()
	{ }

    /// <summary>
    /// Create the DataAccessLayer object. One connection allowed per object
    /// </summary>
    /// <param name="connectionstring">Connection string to the database</param>
    public DataAccessLayer(string connectionstring)
    {
        this.connectionstring = connectionstring;
        connection = new OleDbConnection(connectionstring);
        isOpen = false;

    }

    /// <summary>
    /// Opens the database connection
    /// </summary>
    public void Start()
    {
        if (isOpen)
        {
            return;
        }
        connection.Open();
        isOpen = true;
    }
    /// <summary>
    /// Closes the database connection
    /// </summary>
    public void Stop()
    {
        if (!isOpen)
        {
            return;
        }
        connection.Close();
        isOpen = false;
    }

    /// <summary>
    /// Get a generic object type from a query. 
    /// Note: if there are multiple columns or rows, it will return the first value.
    /// </summary>
    /// <param name="query">An SQL SELECT query</param>
    /// <returns></returns>
    public object GetObject(string query)
    {
        bool wasOpen = isOpen;
        object o = null;
        OleDbCommand cmd = new OleDbCommand(query, connection);
        if (!isOpen)
        {
            Start();
        }
        o = cmd.ExecuteScalar();
        if (!wasOpen)
        {
            Stop();
        }
        return o;
    }
    /// <summary>
    /// Casts the returned value from GetObject to a string.
    /// Helper method for GetObject.
    /// </summary>
    /// <param name="query">An SQL SELECT query</param>
    /// <returns></returns>
    public string GetString(string query)
    {
        return GetObject(query).ToString();
    }
    /// <summary>
    /// Casts the returned value from GetObject to a integer.
    /// Helper method for GetObject.
    /// </summary>
    /// <param name="query">An SQL SELECT query</param>
    /// <returns></returns>
    public int GetInt(string query)
    {
        return int.Parse(GetObject(query).ToString());
    }
    /// <summary>
    /// Casts the returned value from GetObject to a double.
    /// Helper method for GetObject.
    /// </summary>
    /// <param name="query">An SQL SELECT query</param>
    /// <returns></returns>
    public double GetDouble(string query)
    {
        return double.Parse(GetObject(query).ToString());
    }
    /// <summary>
    /// Casts the returned value from GetObject to a boolean.
    /// Helper method for GetObject.
    /// </summary>
    /// <param name="query">An SQL SELECT query</param>
    /// <returns></returns>
    public bool GetBool(string query)
    {
        return bool.Parse(GetObject(query).ToString());
    }
    /// <summary>
    /// Retrieves all data requested from the database as a DataTable
    /// </summary>
    /// <param name="query">An SQL SELECT query</param>
    /// <returns></returns>
    public DataTable GetTable(string query)
    {
        bool wasOpen = isOpen;
        OleDbDataReader dr;
        OleDbCommand cmd = new OleDbCommand(query, connection);
        DataTable dt = new DataTable();
        if (!isOpen)
        {
            Start();
        }
        dr = cmd.ExecuteReader();
        dt.Load(dr);
        if (!wasOpen)
        {
            Stop();
        }
        return dt;
    }
    /// <summary>
    /// Retrieves only the first row of data.
    /// Helper method for GetTable.
    /// </summary>
    /// <param name="query">An SQL SELECT query</param>
    /// <returns></returns>
    public DataRow GetRow(string query)
    {
        DataTable dt = GetTable(query);
        return dt.Rows[0];
    }
    /// <summary>
    /// Retrieves the full set of data requested from the database.
    /// </summary>
    /// <param name="query">An SQL SELECT query</param>
    /// <returns></returns>
    public DataSet GetDataSet(string query)
    {
        bool wasOpen = isOpen;
        OleDbDataAdapter da = new OleDbDataAdapter();
        OleDbCommand cmd = new OleDbCommand(query, connection);
        DataSet ds = new DataSet();
        if (!isOpen)
        {
            Start();
        }
        da.SelectCommand = cmd;
        da.Fill(ds);
        if (!wasOpen)
        {
            Stop();
        }
        return ds;
    }
    /// <summary>
    /// Runs any query against the database.
    /// </summary>
    /// <param name="query">Any SQL query (select, insert, update, delete)</param>
    public void RunQuery(string query)
    {
        bool wasOpen = isOpen;
        OleDbCommand cmd = new OleDbCommand(query, connection);
        if (!isOpen)
        {
            Start();
        }
        //add return catcher
       int n = cmd.ExecuteNonQuery();
       if (n > 0)
       { 
          // trigger event
           if (TableChangeEvent != null)
               TableChangeEvent(this, EventArgs.Empty);
       }

        if (!wasOpen)
        {
            Stop();
        }
    }
    /// <summary>
    /// Inserts data into the database and returns the new primary key.
    /// Since the index can be of any type, this method returns an object.
    /// </summary>
    /// <param name="query">An SQL INSERT query</param>
    /// <returns></returns>
    public object InsertIndex(string query)
    {
        object index = null;
        lock (connection)
        {
            bool wasOpen = isOpen;
            if (!isOpen)
            {
                Start();
            }
            OleDbCommand cmd = new OleDbCommand(query, connection);
            int n = cmd.ExecuteNonQuery();
            if (n == 0)
            {
                throw new InvalidExpressionException("The query did not successfully insert any data");
            }
            
            //Trigger database has been updated event
            //if (TableChangeEvent != null)
              //  TableChangeEvent(this, EventArgs.Empty);


            cmd = new OleDbCommand("SELECT @@IDENTITY", connection);
            index = cmd.ExecuteScalar();
            if (!wasOpen)
            {
                Stop();
            }
        }
        return index;
    }
}
