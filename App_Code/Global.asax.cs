/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Data.OleDb;
using System.Web.Profile;

namespace CST8256Test2 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
            Application.Add("DBConnectionString",
                "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + Server.MapPath(".") + "\\Northwind.mdb");
            Application.Add("DBAccess", new DataAccessLayer((string)Application["DBConnectionString"]));
            
            //Listen for DB changes here
            ((DataAccessLayer)Application["DBAccess"]).TableChangeEvent += new EventTableChangeHandler(updateCatalog);
            //Run initial caching now
            updateCatalog(sender, e);
		}

        protected void updateCatalog(object sender, EventArgs e)
        {
            //Do something here...repopulate the catalogue?
            string selectionString =
                  "SELECT Categories.CategoryName, " +
                      "Products.ProductName, Suppliers.CompanyName, " +
                      "Products.ProductID " +
                      "FROM Categories " +
                      "INNER JOIN (Suppliers " +
                      "INNER JOIN Products " +
                      "ON Suppliers.SupplierID = Products.SupplierID) " +
                      "ON Categories.CategoryID = Products.CategoryID " +
                      "WHERE  Products.UnitsInStock > 0"
                      ;

            Application["Catalog"] = ((DataAccessLayer)Application["DBAccess"]).GetTable(selectionString);
        }

		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{

		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{
            ((OleDbConnection)Application["DBConnection"]).Close();
		}

		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

