﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class NewSupplier : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if ((String)Session["Role"] == "Sales Representative")
        {
            Response.Redirect("Maintenance.aspx", false);
        }

        else if (!IsPostBack)
        {
            
        }

        else
        {
            
        }
    }

    protected void btnSubmitNewSupplier_Click(object sender, EventArgs e)
    {
        if (txtContactName.Text != "" && txtCompanyName.Text != "" && txtContactTitle.Text != "" && txtPhone.Text != "")
        {
            // Make change to database.
            string insertString = "INSERT INTO Suppliers (ContactName, CompanyName, ContactTitle, Phone) VALUES "
                + "('" + txtContactName.Text
                + "', '" + txtCompanyName.Text
                + "', '" + txtContactTitle.Text
                + "', '" + txtPhone.Text + "');";

            string supplierID = (((DataAccessLayer)Application["DBAccess"]).InsertIndex(insertString)).ToString();

            // Update data table.
            DataTable dt = (DataTable)Session["dtSuppliers"];
            DataRow newSupplierRow = dt.NewRow();
            newSupplierRow["SupplierID"] = supplierID;
            newSupplierRow["ContactName"] = txtContactName.Text;
            newSupplierRow["CompanyName"] = txtCompanyName.Text;
            newSupplierRow["ContactTitle"] = txtContactTitle.Text;
            newSupplierRow["Phone"] = txtPhone.Text;
            dt.Rows.Add(newSupplierRow);

            //Persist the table in the Session object.
            Session["dtSuppliers"] = dt;

            //redirect back to supplier page
            Response.Redirect("Suppliers.aspx", true);
        }
        else
        {
            //invalid entry
            if (txtContactName.Text == "")
            {
                txtContactName.Focus();
            }
            else if (txtCompanyName.Text == "")
            {
                txtCompanyName.Focus();
            }
            else if (txtContactTitle.Text == "")
            {
                txtContactTitle.Focus();
            }
            else
            {
                txtPhone.Focus();
            }
        }
    }
}
