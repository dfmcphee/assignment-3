﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaintenanceLayout.master" AutoEventWireup="true" CodeFile="NewShipper.aspx.cs" Inherits="NewShipper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    
    <asp:Label ID="lblCompanyName" runat="server" Text="Company Name: "></asp:Label>
    <asp:TextBox ID="txtCompanyName" runat="server" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must enter a Company Name"
    ControlToValidate="txtCompanyName"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblPhone" runat="server" Text="Phone Number: "></asp:Label>
    <asp:TextBox ID="txtPhone" runat="server" style="margin-left: 9px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must enter a Phone Number"
    ControlToValidate="txtPhone"></asp:RequiredFieldValidator>
    <br />
    <asp:Button ID="btnSubmitNewShipper" runat="server" Text="Create" 
        onclientclick="btnSubmitNewShipper_Click" style="margin-left: 122px" 
        onclick="btnSubmitNewShipper_Click" />
</asp:Content>