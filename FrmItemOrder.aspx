<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CustomerLayout.master" CodeFile="FrmItemOrder.aspx.cs" Inherits="FrmItemOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
       
       <asp:Label ID="Label7" runat="server" Font-Size="Large" Height="32px"  Width="444px">Product Details</asp:Label>
       <br />
       <asp:Label ID="Label1" runat="server" >Label</asp:Label>
       <asp:TextBox ID="TextBox1" runat="server" Enabled="False"  Width="320px" class="number"></asp:TextBox>
       <br />
       <asp:Label ID="Label2" runat="server" >Label</asp:Label>
       <asp:TextBox ID="TextBox2" runat="server" Enabled="False"  Width="320px"></asp:TextBox>
       <br />
       <asp:Label ID="Label3" runat="server" >Label</asp:Label>
       <asp:TextBox ID="TextBox3" runat="server" Enabled="False"  Width="320px"></asp:TextBox>
       <br />
       <asp:Label ID="Label4" runat="server" >Label</asp:Label>
       <asp:TextBox ID="TextBox4" runat="server" Enabled="False"  Width="320px"></asp:TextBox>
       <br />
       <asp:Label ID="Label5" runat="server" >Label</asp:Label>
       <asp:TextBox ID="TextBox5" runat="server" Enabled="False"  Width="320px"></asp:TextBox>
       <br />
       <asp:Label ID="Label6" runat="server" >Label</asp:Label>
       <asp:TextBox ID="TextBox6" runat="server" Enabled="False"  Width="320px" class="number"></asp:TextBox>
       <br />
       <asp:Label ID="LabelStock" runat="server" >Label</asp:Label>
       <asp:TextBox ID="txtStock" runat="server" Enabled="False"  Width="320px" class="number"></asp:TextBox>
       <div runat="server" id="CurrentOrder" visible="false">
        <asp:Label ID="LabelCurrentOrder" runat="server" >Existing Order</asp:Label>
        <asp:TextBox ID="txtCurrentOrder" runat="server" Enabled="False"  Width="320px" class="number"></asp:TextBox>
       </div>
       <br />
       <br />
       <asp:Label ID="Label8" runat="server" >Quanity Desired at QuantityPerUnit</asp:Label>
       <asp:TextBox ID="txtQuantity" runat="server"  Width="320px" class="number"></asp:TextBox>
       <br />
       <asp:Label ID="lblFeedback" runat="server" ForeColor="#CC0000"></asp:Label>
       <br /><br />
       <asp:Button ID="btnReturn" runat="server" OnClick="btnReturn_Click"  Text="Return to Catalog" Width="200px" />
       <asp:Button ID="btnOrder" runat="server" OnClick="btnOrder_Click"  Text="Order" Width="100px" />
       <asp:Button ID="btnMerge" runat="server" onclick="btnReplace_Click" Text="Replace Order" Visible="false" />
       <asp:Button ID="btnAdd" runat="server" onclick="btnAdd_Click"  Text="Add to Order " Visible="false" />
</asp:Content>
