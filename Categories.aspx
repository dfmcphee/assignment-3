﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Categories.aspx.cs" MasterPageFile="~/MaintenanceLayout.master" Inherits="Categories" %>
    
    <asp:content id="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:content>
    
    <asp:content id="Content2" ContentPlaceHolderID="content" runat="server">

        <asp:GridView ID="gvCategories" runat="server" 
            OnRowEditing="gvCategories_EditCommand" 
            OnRowUpdating="gvCategories_UpdateCommand" 
            OnRowCancelingEdit="gvCategories_CancelCommand"
            AutoGenerateColumns="False" DataKey="CategoryID" BackColor="White" 
            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4"
            ForeColor="Black" GridLines="Vertical" RowStyle-Width="960px">
            <RowStyle BackColor="#F7F7DE" />
            <Columns>
                <asp:CommandField ButtonType="Button" ShowEditButton="True" />
                <asp:BoundField DataField="CategoryID" ReadOnly="True" HeaderText="Category ID" />
                <asp:BoundField DataField="CategoryName" HeaderText="Category Name" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:Button ID="btnNewCategory" runat="server" Text="New" 
        style="margin-left: 5px; margin-top: 15px" onclick="btnNewCategory_Click" 
        onclientclick="btnNewCategory_Click" />
    </asp:content>
