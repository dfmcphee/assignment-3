﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MaintenanceLayout.master" CodeFile="EmployeeLogin.aspx.cs" Inherits="Login" %>
    
<asp:content id="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .style1
    {
        height: 53px;
    }
    .style2
    {
        height: 19px;
    }
    .style3
    {
        height: 61px;
    }
    .style4
    {
        height: 54px;
    }
</style>
</asp:content>
    
<asp:content id="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="loginPanel">
        <asp:login runat="server" ID="Login1" onauthenticate="OnAuthenticate" 
            DestinationPageUrl="~/Maintenance.aspx" Height="167px" Width="403px" >
            <LayoutTemplate>
                <table border="0" cellpadding="1" cellspacing="0" 
                    style="border-collapse: collapse;">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" style="height: 166px; width: 402px;">
                                <tr>
                                    <td align="center" colspan="2" class="style3">
                                        Log In</td>
                                </tr>
                                <tr>
                                    <td align="right" class="style4">
                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                                    </td>
                                    <td class="style4">
                                        &nbsp;&nbsp;
                                        <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" 
                                            ControlToValidate="UserName" ErrorMessage="User Name is required." 
                                            ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="style1">
                                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                    </td>
                                    <td class="style1">
                                        &nbsp;&nbsp;
                                        <asp:TextBox ID="Password" runat="server" Height="21px" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" 
                                            ControlToValidate="Password" ErrorMessage="Password is required." 
                                            ToolTip="Password is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" class="style2" colspan="2" style="color: Red;">
                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" 
                                            ValidationGroup="Login1" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
        </asp:login>
    </div>
</asp:content>