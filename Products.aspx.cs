﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Products : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if ((String)Session["Role"] == "Sales Representative")
        {
            Response.Redirect("Maintenance.aspx", false);
        }

        else if (!IsPostBack)
        {
            gvProducts.BorderStyle = BorderStyle.Outset;
            gvProducts.BorderWidth = Unit.Pixel(3);

            DataTable dt;
            string selectionString = "SELECT ProductID, ProductName, UnitPrice FROM Products";

            dt = ((DataAccessLayer)Application["DBAccess"]).GetTable(selectionString);

            gvProducts.DataSource = dt;
            gvProducts.DataBind();

            //Persist the table in the Session object.
            Session["dtProducts"] = dt;
        }

        else
        {
            //Retrieve the table from the session object.
            DataTable dt = (DataTable)Session["dtProducts"];
            gvProducts.DataSource = dt;
        }
    }

    public void gvProducts_EditCommand(object source, GridViewEditEventArgs e)
    {
        //Set the edit index.
        gvProducts.EditIndex = e.NewEditIndex;
        //Bind data to the GridView control.
        gvProducts.DataBind();
    }

    public void gvProducts_UpdateCommand(object source, GridViewUpdateEventArgs e)
    {
        GridViewRow row = gvProducts.Rows[e.RowIndex];

        // Make change to database.
        string updateString = "UPDATE Products SET "
            + "ProductName=" + (((TextBox)(row.Cells[2].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[2].Controls[0])).Text + "'" : "null")
            + ", UnitPrice=" + (((TextBox)(row.Cells[3].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[3].Controls[0])).Text + "'" : "null")
            + " WHERE ProductID=" + row.Cells[1].Text;

        ((DataAccessLayer)Application["DBAccess"]).RunQuery(updateString);

        // Update data table.
        DataTable dt = (DataTable)Session["dtProducts"];
        dt.Rows[row.DataItemIndex]["ProductName"] = ((TextBox)(row.Cells[2].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["UnitPrice"] = ((TextBox)(row.Cells[3].Controls[0])).Text;

        // Switch out of edit mode.
        gvProducts.EditIndex = -1;

        // Bind data.
        gvProducts.DataSource = dt;
        gvProducts.DataBind();

        //Persist the table in the Session object.
        Session["dtProducts"] = dt;
    }

    public void gvProducts_CancelCommand(object source, GridViewCancelEditEventArgs e)
    {
        // Switch out of edit mode.
        gvProducts.EditIndex = -1;

        gvProducts.DataBind();
    }
    protected void btnNewProduct_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewProduct.aspx", true);
    }
}
