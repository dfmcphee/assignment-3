﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Shippers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if ((String)Session["Role"] == "Sales Representative")
        {
            Response.Redirect("Maintenance.aspx", false);
        }

        else if (!IsPostBack)
        {
            gvShippers.BorderStyle = BorderStyle.Outset;
            gvShippers.BorderWidth = Unit.Pixel(3);

            DataTable dt;
            string selectionString = "SELECT ShipperID, CompanyName, Phone FROM Shippers";

            dt = ((DataAccessLayer)Application["DBAccess"]).GetTable(selectionString);

            gvShippers.DataSource = dt;
            gvShippers.DataBind();

            //Persist the table in the Session object.
            Session["dtShippers"] = dt;
        }

        else
        {
            //Retrieve the table from the session object.
            DataTable dt = (DataTable)Session["dtShippers"];
            gvShippers.DataSource = dt;
        }
    }

    public void gvShippers_EditCommand(object source, GridViewEditEventArgs e)
    {
        //Set the edit index.
        gvShippers.EditIndex = e.NewEditIndex;
        //Bind data to the GridView control.
        gvShippers.DataBind();
    }

    public void gvShippers_UpdateCommand(object source, GridViewUpdateEventArgs e)
    {
        GridViewRow row = gvShippers.Rows[e.RowIndex];

        // Make change to database.
        string updateString = "UPDATE Shippers SET "
            + "CompanyName=" + (((TextBox)(row.Cells[2].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[2].Controls[0])).Text + "'" : "null")
            + ", Phone=" + (((TextBox)(row.Cells[3].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[3].Controls[0])).Text + "'" : "null")
            + " WHERE ShipperID=" + row.Cells[1].Text;

        ((DataAccessLayer)Application["DBAccess"]).RunQuery(updateString);

        // Update data table.
        DataTable dt = (DataTable)Session["dtShippers"];
        dt.Rows[row.DataItemIndex]["CompanyName"] = ((TextBox)(row.Cells[2].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["Phone"] = ((TextBox)(row.Cells[3].Controls[0])).Text;

        // Switch out of edit mode.
        gvShippers.EditIndex = -1;

        // Bind data.
        gvShippers.DataSource = dt;
        gvShippers.DataBind();

        //Persist the table in the Session object.
        Session["dtShippers"] = dt;
    }

    public void gvShippers_CancelCommand(object source, GridViewCancelEditEventArgs e)
    {
        // Switch out of edit mode.
        gvShippers.EditIndex = -1;

        gvShippers.DataBind();
    }
    protected void btnNewShipper_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewShipper.aspx", true);
    }
}
