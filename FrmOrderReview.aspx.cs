/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Data.OleDb;

public partial class FrmOrderReview : System.Web.UI.Page
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        //redirect to CustomerLogin.aspx if user has not signed in
        if (Session["cart"] == null || Profile.Email == "")
        {
            Server.Transfer("CustomerLogin.aspx");
        }

        DataTable theCart = (DataTable)(Session["cart"]);
        showOrder(theCart.Rows);

    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Session.RemoveAll();
        Server.Transfer("CustomerLogin.aspx");  
    }

    /// <summary>
    /// Code missing here
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, System.EventArgs e)
    {
        bool stockError = false;
        // 1) check quantities
        ICollection values = ((DataTable)Session["cart"]).Rows;
        string orders = "Product Name".PadRight(50)
                        +"Quantity".PadRight(15)
                        +"Price\n";
        double total = 0;

        int[] quantityOnHand = new int[values.Count]; 

        int i = 0;

        DataAccessLayer dba = (DataAccessLayer)Application["DBAccess"];

        foreach (DataRow item in values)
        {
            orders += item["name"].ToString().PadRight(50)
                        + item["quantity"].ToString().PadLeft(15)
                        + double.Parse(item["price"].ToString()).ToString("C2").PadLeft(15) + "\n";
            total += double.Parse(item["price"].ToString()) * int.Parse(item["quantity"].ToString());
            string productsQuery = "SELECT UnitsInStock FROM Products WHERE ProductID = "+ item["id"] + ";";

            quantityOnHand[i] = dba.GetInt(productsQuery);

            // check if there is enough stock
            if (quantityOnHand[i] < int.Parse(item["quantity"].ToString()))
            {
                stockError = true;
                break;
            }
            i++;
        } // end for each

        // if there was enough stock, proceed with order
        if (!stockError)
        {
            // 2) prepare the orders
            Random random = new Random();
            int customerID = random.Next(0,9999);
            // insert order into db
            string orderID = dba.InsertIndex("INSERT INTO Orders (OrderDate) VALUES ('" + DateTime.Today.ToString() + "');").ToString();

            int index = 0;
            //update stock totals
            foreach (DataRow item in values)
            {
                // insert product order into db
                String insertProdCmd = "INSERT INTO [Order Details] (ProductID, OrderID, Quantity, UnitPrice)"
                   + "VALUES (" + item["id"] + ", " + orderID + ", "
                   + item["quantity"] + ", " + item["price"] + ");";

                dba.RunQuery(insertProdCmd);

                // minus quanity ordered from stock total
                quantityOnHand[index] -= int.Parse(item["quantity"].ToString());

                // update db with new total
                String updateCmd = "UPDATE Products SET UnitsInStock = " + 
                    quantityOnHand[index] + " WHERE ProductID =" + item["id"] + ";";
                dba.RunQuery(updateCmd);
            }

            // 3) prepare email
            string email = WebConfigurationManager.AppSettings["Email"];
            string pass = WebConfigurationManager.AppSettings["Password"];
            string host = WebConfigurationManager.AppSettings["Host"];

            int port = 23;
            if (!int.TryParse(WebConfigurationManager.AppSettings["Port"], out port))
            {
                port = 23;
            }
            bool ssl = WebConfigurationManager.AppSettings["SSL"] == "ON";
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(email);
            msg.To.Add(new MailAddress(Profile.Email));
            msg.Subject = "Order Confirmed";
            msg.Body = "This email has been sent to confirm your order.\n\n" + orders
                        + "\nTotal" + total.ToString("C2").PadLeft(65);

            SmtpClient client = new SmtpClient(host, port);
            client.EnableSsl = ssl;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(email, pass);
            try
            {
                client.Send(msg); // no good way to do this with the redirect at the moment.
            }
            catch (Exception) { }
            // 4) send email
            //throw new Exception("Do Something to me here");
            // Check quantities available
            // Build and send email to customer
            // Confirming all in stock items and new total

            // clear all session variables
            Session.RemoveAll();
            // return to start page
            Response.Clear();
            Response.Redirect("FrmOrderConfirmation.aspx", true); // CST8256TestStart
            // There is a bug here 
            // causing this page to display 
            // with the transferred page
        }
        else
        {
           lblFeedback.Text = "There is not enough stock to complete your order.";
        }
    }

    /// <summary>
    /// showOrder displays the OrderItems in a table
    /// using HTMLTextWriter
    /// Adapted from Russell Text chapter 10
    /// </summary>
    /// <param name="values"></param>
    private void showOrder(IEnumerable values)
    {
        double total = 0;

        HtmlTable tbl = new HtmlTable();
        //StringBuilder sb = new StringBuilder(3000);
        //StringWriter sw = new StringWriter(sb);
       // HtmlTextWriter htw = new HtmlTextWriter(sw);

        HtmlTableRow row;
        HtmlTableCell cell;
        tbl.Border = 1;
        tbl.Align = "left";

        // Now add a Cell for each property and item
        // there are 5 cells per row
        foreach (DataRow item in values)
        {
            row = new HtmlTableRow();
            tbl.Rows.Add(row);

            cell = new HtmlTableCell();
            cell.Attributes.Add("style",
                "color:black; " +
                "background:lightYellow");
            cell.Align = "Right";
            cell.InnerText = item["id"].ToString();
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Attributes.Add("style", "color:blue; background:lightYellow");
            cell.Align = "Left";
            cell.InnerText = item["name"].ToString();
            row.Cells.Add(cell);

            double price = double.Parse(item["price"].ToString());
            int quantity = int.Parse(item["quantity"].ToString());
            double lineTotal = Math.Round(price * quantity, 2);
            total += lineTotal;

            cell = new HtmlTableCell();
            cell.Attributes.Add("style",
                "color:black; " +
                "background:lightYellow");
            cell.Align = "Right";
            cell.InnerText = quantity.ToString();
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Attributes.Add("style",
                "color:black; " +
                "background:lightYellow");
            cell.Align = "Right";
            cell.InnerText = price.ToString("F");
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Attributes.Add("style",
                "color:black; " +
                "background:lightYellow");
            cell.Align = "Right";
            cell.InnerText = lineTotal.ToString("C");
            row.Cells.Add(cell);

            cell = new HtmlTableCell();

            Button newButton = new Button();
            newButton.ID = item["id"].ToString();
            newButton.Text = "Delete";
            newButton.UseSubmitBehavior = true;
            newButton.Click += new EventHandler(newButton_Click);
            newButton.Visible = true;
            cell.Controls.Add(newButton);
            row.Cells.Add(cell);
        }

        // Now we need the Total
        // Mostly an empty row with nothing except the last cell
        row = new HtmlTableRow();
        tbl.Rows.Add(row);
        // deadcell 1
        cell = new HtmlTableCell();
        row.Cells.Add(cell);
        // deadcell 2
        cell = new HtmlTableCell();
        row.Cells.Add(cell);
        // deadcell 3
        cell = new HtmlTableCell();
        row.Cells.Add(cell);
        // deadcell 4
        cell = new HtmlTableCell();
        row.Cells.Add(cell);

        // total cell
        cell = new HtmlTableCell();
        cell.Attributes.Add("style",
            "color:black; " +
            "background:lightYellow");
        cell.Align = "Right";
        cell.InnerText = total.ToString("C");
        row.Cells.Add(cell);
        tbl.EnableViewState = true;
        PlaceHolder1.Controls.Add(tbl);
        
    }
    protected void newButton_Click(object sender, System.EventArgs e)
    {
        PlaceHolder1.Controls.Clear();
        DataTable cart = (DataTable)Session["cart"];
        cart.Rows.Remove(cart.Rows.Find(new object[] { int.Parse(((Button)sender).ID) }));
        lblFeedback.Text = "Item removed from Cart.<br/>";
        showOrder(cart.Rows);
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Server.Transfer("CatalogDisplay.aspx");
    }
}
