<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CustomerLayout.master" CodeFile="CatalogDisplay.aspx.cs" Inherits="CatalogDisplay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
       <div id="info" runat="server"></div>
       <asp:Panel ID="Panel1" runat="server" Height="50px" Width="526px">
       <% if (Profile.Email != "")
          { %>
            <asp:Button ID="btnCheckout" runat="server" OnClick="btnCheckout_Click" Text="Review and Checkout" Width="200px" />
       <% }
          else
          { %>
           <asp:LinkButton ID="LoginButton" runat="server" PostBackUrl="~/CustomerLogin.aspx">You must login before you can checkout.</asp:LinkButton>
       <% } %>
       <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel Order" Width="100px" />
       </asp:Panel>
       &nbsp;
       <asp:DataGrid ID="dgProducts" runat="server" BackColor="White" BorderColor="#DEDFDE"
          BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" Height="123px"
          OnSelectedIndexChanged="dgProducts_SelectedIndexChanged" PageSize="3" 
           AllowSorting="True" OnSortCommand="dgProducts_sortClick" 
           AutoGenerateColumns="False" GridLines="Vertical">
           <FooterStyle BackColor="#CCCC99" />
           <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
           <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
               Mode="NumericPages" />
           <AlternatingItemStyle BackColor="White" />
           <ItemStyle BackColor="#F7F7DE" />
          <Columns>
             <asp:ButtonColumn CommandName="Select" HeaderText="Click to view" Text="View Details">
                <ItemStyle Wrap="False" />
             </asp:ButtonColumn>
             <asp:BoundColumn DataField="CategoryName" ReadOnly="True" SortExpression="CategoryName" HeaderText="Category Name">
             </asp:BoundColumn> 
             <asp:BoundColumn DataField="ProductName" ReadOnly="True" SortExpression="ProductName" HeaderText="Product Name">
             </asp:BoundColumn> 
             <asp:BoundColumn DataField="CompanyName" ReadOnly="True" SortExpression="CompanyName" HeaderText="Company Name">
             </asp:BoundColumn> 
             <asp:BoundColumn DataField="ProductId" ReadOnly="True" SortExpression="ProductId" HeaderText="Product ID">
             </asp:BoundColumn> 
          </Columns>
           <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
       </asp:DataGrid>
</asp:Content>