﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MaintenanceLayout.master" CodeFile="Customers.aspx.cs" Inherits="Customers" %>
    
<asp:content id="Content1" ContentPlaceHolderID="head" runat="server">
</asp:content>

<asp:content id="Content2" ContentPlaceHolderID="content" runat="server">
         <asp:GridView ID="gvCustomers" runat="server" 
             OnRowEditing="gvCustomers_EditCommand" 
             OnRowUpdating="gvCustomers_UpdateCommand" 
             OnRowCancelingEdit="gvCustomers_CancelCommand" 
             AutoGenerateColumns="False" DataKey="CustomerID" BackColor="White" 
             BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
             ForeColor="Black" GridLines="Vertical">
             
             <RowStyle BackColor="#F7F7DE" />
             
            <Columns>
                <asp:CommandField ButtonType="Button" ShowEditButton="True" />
                <asp:BoundField DataField="CustomerID" ReadOnly="True" HeaderText="CustomerID" />
                <asp:BoundField DataField="ContactName" HeaderText="Contact Name" />
                <asp:BoundField DataField="CompanyName" HeaderText="Company Name" />
                <asp:BoundField DataField="ContactTitle" HeaderText="Contact Title" />
                <asp:BoundField DataField="Phone" HeaderText="Phone" />
                <asp:BoundField DataField="Fax" HeaderText="Fax" />  
            </Columns>
            
             <FooterStyle BackColor="#CCCC99" />
             <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
             <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
             <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
             <AlternatingRowStyle BackColor="White" />
            
        </asp:GridView>
        <asp:Button ID="btnNewCustomer" runat="server" Text="New" 
        style="margin-left: 5px; margin-top: 15px" 
    onclick="btnNewCustomer_Click" onclientclick="btnNewCustomer_Click" />

</asp:content>
