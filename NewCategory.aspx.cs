﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class NewCategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if ((String)Session["Role"] == "Sales Representative")
        {
            Response.Redirect("Maintenance.aspx", false);
        }

        else if (!IsPostBack)
        {
            
        }

        else
        {
            
        }
    }

    protected void btnSubmitNewCategory_Click(object sender, EventArgs e)
    {
        if(txtCategoryName.Text != "" && txtDescription.Text != ""){
            // Make change to database.
            string insertString = "INSERT INTO Categories (CategoryName, Description) VALUES "
                + "('" + txtCategoryName.Text
                + "', '" + txtDescription.Text + "');";

            string categoryID = (((DataAccessLayer)Application["DBAccess"]).InsertIndex(insertString)).ToString();

            // Update data table.
            DataTable dt = (DataTable)Session["dtCategories"];
            DataRow newCategoryRow = dt.NewRow();
            newCategoryRow["CategoryID"] = categoryID;
            newCategoryRow["CategoryName"] = txtCategoryName.Text;
            newCategoryRow["Description"] = txtDescription.Text;
            dt.Rows.Add(newCategoryRow);

            //Persist the table in the Session object.
            Session["dtCategories"] = dt;
            
            //redirect back to category page
            Response.Redirect("Categories.aspx", true);
        }
        else{
            //invalid entry
            if (txtCategoryName.Text == "")
            {
                txtCategoryName.Focus();
            }
            else
            {
                txtDescription.Focus();
            }

        }
    }
}
