﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class NewEmployee : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if (!IsPostBack)
        {
            
        }

        else
        {
            
        }
    }

    protected void btnSubmitNewEmployee_Click(object sender, EventArgs e)
    {
        if (txtLastName.Text != "" && txtFirstName.Text != "" && txtTitle.Text != "" && txtAddress.Text != "" && txtCity.Text != "" && txtRegion.Text != "" && txtPostalCode.Text != "" && txtHomePhone.Text != "" && txtExtension.Text != "")
        {
            // Make change to database.
            string insertString = "INSERT INTO Employees (LastName, FirstName, Title, Address, City, Region, PostalCode, HomePhone, Extension) VALUES "
                + "('" + txtLastName.Text
                + "', '" + txtFirstName.Text
                + "', '" + txtTitle.Text
                + "', '" + txtAddress.Text
                + "', '" + txtCity.Text
                + "', '" + txtRegion.Text
                + "', '" + txtPostalCode.Text
                + "', '" + txtHomePhone.Text
                + "', '" + txtExtension.Text + "');";

            string employeeID = (((DataAccessLayer)Application["DBAccess"]).InsertIndex(insertString)).ToString();

            // Update data table.
            DataTable dt = (DataTable)Session["dtEmployees"];
            DataRow newEmployeeRow = dt.NewRow();
            newEmployeeRow["EmployeeID"] = employeeID;
            newEmployeeRow["LastName"] = txtLastName.Text;
            newEmployeeRow["FirstName"] = txtFirstName.Text;
            newEmployeeRow["Title"] = txtTitle.Text;
            newEmployeeRow["Address"] = txtAddress.Text;
            newEmployeeRow["City"] = txtCity.Text;
            newEmployeeRow["Region"] = txtRegion.Text;
            newEmployeeRow["PostalCode"] = txtPostalCode.Text;
            newEmployeeRow["HomePhone"] = txtHomePhone.Text;
            newEmployeeRow["Extension"] = txtExtension.Text;
            dt.Rows.Add(newEmployeeRow);

            //Persist the table in the Session object.
            Session["dtEmployees"] = dt;

            //redirect back to employee page
            Response.Redirect("Employees.aspx", true);
        }
        else
        {
            //invalid entry
            if (txtLastName.Text == "")
            {
                txtLastName.Focus();
            }
            else if (txtFirstName.Text == "")
            {
                txtFirstName.Focus();
            }
            else if (txtTitle.Text == "")
            {
                txtTitle.Focus();
            }
            else if (txtAddress.Text == "")
            {
                txtAddress.Focus();
            }
            else if (txtCity.Text == "")
            {
                txtCity.Focus();
            }
            else if (txtRegion.Text == "")
            {
                txtRegion.Focus();
            }
            else if (txtPostalCode.Text == "")
            {
                txtPostalCode.Focus();
            }
            else if (txtHomePhone.Text == "")
            {
                txtHomePhone.Focus();
            }
            else
            {
                txtExtension.Focus();
            }
        }
    }
}