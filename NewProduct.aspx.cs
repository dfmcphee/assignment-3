﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class NewProduct : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if ((String)Session["Role"] == "Sales Representative")
        {
            Response.Redirect("Maintenance.aspx", false);
        }

        else if (!IsPostBack)
        {
            
        }

        else
        {
            
        }
    }

    protected void btnSubmitNewProduct_Click(object sender, EventArgs e)
    {
        if (txtProductName.Text != "" && txtUnitPrice.Text != "")
        {
            // Make change to database.
            string insertString = "INSERT INTO Products (ProductName, UnitPrice) VALUES "
                + "('" + txtProductName.Text
                + "', '" + txtUnitPrice.Text + "');";

            string productID = (((DataAccessLayer)Application["DBAccess"]).InsertIndex(insertString)).ToString();

            // Update data table.
            DataTable dt = (DataTable)Session["dtProducts"];
            DataRow newProductRow = dt.NewRow();
            newProductRow["ProductID"] = productID;
            newProductRow["ProductName"] = txtProductName.Text;
            newProductRow["UnitPrice"] = txtUnitPrice.Text;
            dt.Rows.Add(newProductRow);

            //Persist the table in the Session object.
            Session["dtProducts"] = dt;

            //redirect back to product page
            Response.Redirect("Products.aspx", true);
        }
        else
        {
            //invalid entry
            if (txtProductName.Text == "")
            {
                txtProductName.Focus();
            }
            else
            {
                txtUnitPrice.Focus();
            }
        }
    }
}
