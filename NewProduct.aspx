﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaintenanceLayout.master" AutoEventWireup="true" CodeFile="NewProduct.aspx.cs" Inherits="NewProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    
    <asp:Label ID="lblProductName" runat="server" Text="Product Name: "></asp:Label>
    <asp:TextBox ID="txtProductName" runat="server" style="margin-left: 2px" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must enter a Product Name"
    ControlToValidate="txtProductName"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price: "></asp:Label>
    <asp:TextBox ID="txtUnitPrice" runat="server" style="margin-left: 34px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must enter a Unit Price"
    ControlToValidate="txtUnitPrice"></asp:RequiredFieldValidator>
    <br />
    <asp:Button ID="btnSubmitNewProduct" runat="server" Text="Create" 
        style="margin-left: 110px" 
        onclick="btnSubmitNewProduct_Click" />
</asp:Content>