/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class CatalogDisplay : System.Web.UI.Page
{
    // a constant to manage the cell number that contains the Product ID
    private const int PRODUCT_ID_CELL_INDEX = 4;


    protected void Page_Load(object sender, System.EventArgs e)
    {
        dgProducts.BorderStyle = BorderStyle.Outset;
        dgProducts.BorderWidth = Unit.Pixel(3);

        DataTable dt;
        if (Application["Catalog"] == null)
        {
            throw new Exception("Catalog has not been cached");
        }
        else
        {
            dt = (DataTable)Application["Catalog"];
        }
        if (Session["SortExp"] != null && Session["SortDir"] != null)
        {
            dt.DefaultView.Sort = Session["SortExp"] + " " + Session["SortDir"];
        }
        else
        {
            dt.DefaultView.Sort = null;
        }
        dgProducts.DataSource = dt;
        dgProducts.DataBind();
    }

    /// <summary>
    /// View Details was clicked for a product
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dgProducts_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        // Capture selected Product ID using Selected Row index and fixed Cell index
        Session.Add("CurrentItem",
            dgProducts.Items[dgProducts.SelectedIndex]
                .Cells[PRODUCT_ID_CELL_INDEX].Text);
        //  display a product page for that item
        // Page will read the Session variable in its Page_Load and get necessary data
        Server.Transfer("FrmItemOrder.aspx");
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Session.RemoveAll(); // otherwise infinite loop
        Server.Transfer("CustomerLogin.aspx"); // ??
    }

    protected void btnCheckout_Click(object sender, System.EventArgs e)
    {
        if (Session["cart"] == null)
        {
            info.InnerText = "You have not selected any items. Please select an item before checking out.";
            return;
        }
        Response.Clear();
        //Server.Transfer("FrmOrderReview.aspx");
        Response.Redirect("FrmOrderReview.aspx", true);
    }

    protected void dgProducts_sortClick(object sender, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
    {
        string sortExpression = (string)Session["SortExp"];
        string sortDirection = (string)Session["SortDir"];

      if(sortExpression != e.SortExpression)
      {
        sortExpression = e.SortExpression;
        sortDirection = "asc";
      }
      else
      {
        if(sortDirection == "asc")
          sortDirection = "desc";
        else
          sortDirection = "asc";
      }

      Session["SortExp"] = sortExpression;
      Session["SortDir"] = sortDirection;
      Session["newSort"] = true;
      Response.Clear();
      Page_Load(sender, e);
    }


}
