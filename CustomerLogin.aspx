<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CustomerLayout.master" CodeFile="CustomerLogin.aspx.cs" Inherits="StartPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    <div id="loginPanel">
       <asp:Label ID="lblEMailPrompt" runat="server"> Enter the email that is to receive final confirmation</asp:Label><br />
       <asp:TextBox ID="txtEMail" runat="server"></asp:TextBox><br />
       <asp:Button ID="btnViewCatalog" runat="server" OnClick="btnViewCatalog_Click" text="Login" />
    </div>
</asp:Content>