﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class NewCustomer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if (!IsPostBack)
        {
            
        }

        else
        {
            
        }
    }

    protected void btnSubmitNewCustomer_Click(object sender, EventArgs e)
    {
        if (txtContactName.Text != "" && txtCompanyName.Text != "" && txtContactTitle.Text != "" && txtPhone.Text != "" && txtFax.Text != "")
        {
            // Make change to database.
            string insertString = "INSERT INTO Customers (CustomerID, ContactName, CompanyName, ContactTitle, Phone, Fax) VALUES "
                + "('" + (txtCompanyName.Text.Substring(0, 5)).ToUpper()
                + "', '" + txtContactName.Text
                + "', '" + txtCompanyName.Text
                + "', '" + txtContactTitle.Text
                + "', '" + txtPhone.Text
                + "', '" + txtFax.Text + "');";

            string customerID = (((DataAccessLayer)Application["DBAccess"]).InsertIndex(insertString)).ToString();

            // Update data table.
            DataTable dt = (DataTable)Session["dtCustomers"];
            DataRow newCustomerRow = dt.NewRow();
            newCustomerRow["CustomerID"] = customerID;
            newCustomerRow["ContactName"] = txtContactName.Text;
            newCustomerRow["CompanyName"] = txtCompanyName.Text;
            newCustomerRow["ContactTitle"] = txtContactTitle.Text;
            newCustomerRow["Phone"] = txtPhone.Text;
            newCustomerRow["Fax"] = txtFax.Text;
            dt.Rows.Add(newCustomerRow);

            //Persist the table in the Session object.
            Session["dtCustomers"] = dt;

            //redirect back to customer page
            Response.Redirect("Customers.aspx", true);
        }
        else
        {
            //invalid entry
            if (txtContactName.Text == "")
            {
                txtContactName.Focus();
            }
            else if (txtCompanyName.Text == "")
            {
                txtCompanyName.Focus();
            }
            else if (txtContactTitle.Text == "")
            {
                txtContactTitle.Focus();
            }
            else if (txtPhone.Text == "")
            {
                txtPhone.Focus();
            }
            else
            {
                txtFax.Focus();
            }
        }
    }
}