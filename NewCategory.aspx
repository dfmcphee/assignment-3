﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaintenanceLayout.master" AutoEventWireup="true" CodeFile="NewCategory.aspx.cs" Inherits="NewCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    
    <asp:Label ID="lblCategoryName" runat="server" Text="Category Name: "></asp:Label>
    <asp:TextBox ID="txtCategoryName" runat="server" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must enter a Category Name"
    ControlToValidate="txtCategoryName"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblDescription" runat="server" Text="Description: "></asp:Label>
    <asp:TextBox ID="txtDescription" runat="server" style="margin-left: 30px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must enter a Description"
    ControlToValidate="txtDescription"></asp:RequiredFieldValidator>
    <br />
    <asp:Button ID="btnSubmitNewCategory" runat="server" Text="Create" 
        style="margin-left: 118px" onclick="btnSubmitNewCategory_Click" />
</asp:Content>