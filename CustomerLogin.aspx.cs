/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

public partial class StartPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       txtEMail.Focus();
    }
    protected void btnViewCatalog_Click(object sender, EventArgs e)
    {
        // validate email address using regular expression

        if (IsValidEmail(txtEMail.Text))
        {
            // store valid email in Profile variable
            Profile.Email = txtEMail.Text;
            // display catalog page
            Server.Transfer("CatalogDisplay.aspx");
        }
        else
        {
            // Add message and redisplay this page
            Response.Write("You must enter a valid email address to continue");
            // set focus to email field
        }
    }

    // from MSDN .NET Framework Developer's Guide
    private bool IsValidEmail(string strIn)
    {
        // Return true if strIn is in valid e-mail format.
        return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
    }
}
