﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Suppliers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] == null)
        {
            Response.Redirect("EmployeeLogin.aspx");
        }

        else if ((String)Session["Role"] == "Sales Representative")
        {
            Response.Redirect("Maintenance.aspx", false);
        }

        else if (!IsPostBack)
        {
            gvSuppliers.BorderStyle = BorderStyle.Outset;
            gvSuppliers.BorderWidth = Unit.Pixel(3);

            DataTable dt;
            string selectionString = "SELECT SupplierID, CompanyName, ContactName, ContactTitle, Phone FROM Suppliers";

            dt = ((DataAccessLayer)Application["DBAccess"]).GetTable(selectionString);

            gvSuppliers.DataSource = dt;
            gvSuppliers.DataBind();

            //Persist the table in the Session object.
            Session["dtSuppliers"] = dt;
        }

        else
        {
            //Retrieve the table from the session object.
            DataTable dt = (DataTable)Session["dtSuppliers"];
            gvSuppliers.DataSource = dt;
        }
    }

    public void gvSuppliers_EditCommand(object source, GridViewEditEventArgs e)
    {
        //Set the edit index.
        gvSuppliers.EditIndex = e.NewEditIndex;
        //Bind data to the GridView control.
        gvSuppliers.DataBind();
    }

    public void gvSuppliers_UpdateCommand(object source, GridViewUpdateEventArgs e)
    {
        GridViewRow row = gvSuppliers.Rows[e.RowIndex];

        string updateString = "UPDATE Suppliers SET "
            + "CompanyName=" + (((TextBox)(row.Cells[2].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[2].Controls[0])).Text + "'" : "null")
            + ", ContactName=" + (((TextBox)(row.Cells[3].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[3].Controls[0])).Text + "'" : "null")
            + ", ContactTitle=" + (((TextBox)(row.Cells[4].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[4].Controls[0])).Text + "'" : "null")
            + ", Phone=" + (((TextBox)(row.Cells[5].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[5].Controls[0])).Text + "'" : "null")
            + " WHERE SupplierID=" + row.Cells[1].Text;

        ((DataAccessLayer)Application["DBAccess"]).RunQuery(updateString);

        // Update data table.
        DataTable dt = (DataTable)Session["dtSuppliers"];
        dt.Rows[row.DataItemIndex]["CompanyName"] = ((TextBox)(row.Cells[2].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["ContactName"] = ((TextBox)(row.Cells[3].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["ContactTitle"] = ((TextBox)(row.Cells[4].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["Phone"] = ((TextBox)(row.Cells[5].Controls[0])).Text;

        // Switch out of edit mode.
        gvSuppliers.EditIndex = -1;

        // Bind data.
        gvSuppliers.DataSource = dt;
        gvSuppliers.DataBind();

        //Persist the table in the Session object.
        Session["dtSuppliers"] = dt;
    }

    public void gvSuppliers_CancelCommand(object source, GridViewCancelEditEventArgs e)
    {
        // Switch out of edit mode.
        gvSuppliers.EditIndex = -1;

        gvSuppliers.DataBind();
    }
    protected void btnNewSupplier_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewSupplier.aspx", true);
    }
}
