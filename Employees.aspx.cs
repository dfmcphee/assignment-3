﻿/********************************
 * Michael Hall                 *
 * Dominic McPhee               *
 * Mike Dalziel                 *
 * Ben Sobol                    *
 * Raphael Gera                 *
 * Gregor Terrill               *
 *                              *
 * Assignment 4                 *
 * DbAccessDemo                 *
 * CST 8256                     *
 * November, 2011               *
 ********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Employees : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((String)Session["Role"] != "Vice President, Sales")
        {
            Response.Redirect("Maintenance.aspx");
        }
        
        else if (!IsPostBack)
        {
            gvEmployees.BorderStyle = BorderStyle.Outset;
            gvEmployees.BorderWidth = Unit.Pixel(3);

            DataTable dt;
            string selectionString = "SELECT EmployeeID, LastName, FirstName, Title, "
            + "Address, City, Region, PostalCode, HomePhone, BirthDate, HireDate FROM Employees";

            dt = ((DataAccessLayer)Application["DBAccess"]).GetTable(selectionString);

            gvEmployees.DataSource = dt;
            gvEmployees.DataBind();

            //Persist the table in the Session object.
            Session["dtEmployees"] = dt;
        }
        else
        {
            //Retrieve the table from the session object.
            DataTable dt = (DataTable)Session["dtEmployees"];
            gvEmployees.DataSource = dt;
        }
    }

    public void gvEmployees_EditCommand(object source, GridViewEditEventArgs e)
    {
        //Set the edit index.
        gvEmployees.EditIndex = e.NewEditIndex;
        //Bind data to the GridView control.
        gvEmployees.DataBind();
    }

    public void gvEmployees_UpdateCommand(object source, GridViewUpdateEventArgs e)
    {
        GridViewRow row = gvEmployees.Rows[e.RowIndex];

        // Make change to database.
        string updateString = "UPDATE Employees SET "
            + "LastName=" + (((TextBox)(row.Cells[2].Controls[0])).Text != "" ? "'"+((TextBox)(row.Cells[2].Controls[0])).Text+"'" : "null")
            + ", FirstName=" + (((TextBox)(row.Cells[3].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[3].Controls[0])).Text + "'" : "null")
            + ", Title=" + (((TextBox)(row.Cells[4].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[4].Controls[0])).Text + "'" : "null")
            + ", Address=" + (((TextBox)(row.Cells[5].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[5].Controls[0])).Text + "'" : "null")
            + ", City=" + (((TextBox)(row.Cells[6].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[6].Controls[0])).Text + "'" : "null")
            + ", Region=" + (((TextBox)(row.Cells[7].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[7].Controls[0])).Text + "'" : "null")
            + ", PostalCode=" + (((TextBox)(row.Cells[8].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[8].Controls[0])).Text + "'" : "null")
            + ", HomePhone=" + (((TextBox)(row.Cells[9].Controls[0])).Text != "" ? "'" + ((TextBox)(row.Cells[9].Controls[0])).Text + "'" : "null")
            + " WHERE EmployeeID=" + row.Cells[1].Text;

        ((DataAccessLayer)Application["DBAccess"]).RunQuery(updateString);

        // Update data table.
        DataTable dt = (DataTable)Session["dtEmployees"];
        dt.Rows[row.DataItemIndex]["LastName"] = ((TextBox)(row.Cells[2].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["FirstName"] = ((TextBox)(row.Cells[3].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["Title"] = ((TextBox)(row.Cells[4].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["Address"] = ((TextBox)(row.Cells[5].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["City"] = ((TextBox)(row.Cells[6].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["Region"] = ((TextBox)(row.Cells[7].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["PostalCode"] = ((TextBox)(row.Cells[8].Controls[0])).Text;
        dt.Rows[row.DataItemIndex]["HomePhone"] = ((TextBox)(row.Cells[9].Controls[0])).Text;

        // Switch out of edit mode.
        gvEmployees.EditIndex = -1;

        // Bind data.
        gvEmployees.DataSource = dt;
        gvEmployees.DataBind();

        //Persist the table in the Session object.
        Session["dtEmployees"] = dt;
    }

    public void gvEmployees_CancelCommand(object source, GridViewCancelEditEventArgs e)
    {
        // Switch out of edit mode.
        gvEmployees.EditIndex = -1;

        gvEmployees.DataBind();
    }


    protected void btnNewEmployee_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewEmployee.aspx", true);
    }
}
