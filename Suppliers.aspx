﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaintenanceLayout.master" AutoEventWireup="true" CodeFile="Suppliers.aspx.cs" Inherits="Suppliers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    <asp:GridView ID="gvSuppliers" runat="server" 
            OnRowEditing="gvSuppliers_EditCommand" 
            OnRowUpdating="gvSuppliers_UpdateCommand" 
            OnRowCancelingEdit="gvSuppliers_CancelCommand"
            AutoGenerateColumns="False" DataKey="ProductID" BackColor="White" 
            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4"
            ForeColor="Black" GridLines="Vertical">
            <RowStyle BackColor="#F7F7DE" />
            <Columns>
                <asp:CommandField ButtonType="Button" ShowEditButton="True" />
                <asp:BoundField DataField="SupplierID" ReadOnly="True" HeaderText="Supplier ID" />
                <asp:BoundField DataField="CompanyName" HeaderText="Company Name" />
                <asp:BoundField DataField="ContactName" HeaderText="Contact Name" />
                <asp:BoundField DataField="ContactTitle" HeaderText="Contact Title" />
                <asp:BoundField DataField="Phone" HeaderText="Phone" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:Button ID="btnNewSupplier" runat="server" Text="New" 
        style="margin-left: 5px; margin-top: 15px" 
    onclick="btnNewSupplier_Click" onclientclick="btnNewSupplier_Click" />
</asp:Content>